thekraf
=======

.. _Bootstrap 3: http://getbootstrap.com
.. _CoffeeScript: http://coffeescript.org
.. _deploying-flask: http://flask.pocoo.org/docs/0.10/deploying/
.. _ev-paper: https://
    www.eecis.udel.edu/~mckennar/academics/gametheory/farkle.pdf
.. _Farkle: https://en.wikipedia.org/wiki/Farkle
.. _Farkle Rules: https://
    cdn.shptrn.com/media/mfg/1725/media_document/8976/FarkleRules_E.pdf
.. _Flask: http://flask.pocoo.org
.. _Hamlish-jinja: https://github.com/Pitmairen/hamlish-jinja
.. _Python 3: https://www.python.org
.. _Read the Docs: http://thekraf.readthedocs.org/en/latest
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Sphinx: http://www.sphinx-doc.org/en/stable/index.html
.. _SQLAlchemy: http://www.sqlalchemy.org
.. _sqlite: https://www.sqlite.org
.. _webpack: https://webpack.github.io

.. _purpose:

Purpose
-------

Aid deep probablistic/statistical analysis of Farkle_, including a web-based,
multi-player implementation of Farkle for reference.

.. _quickstart:

Quickstart
----------

Want to get rolling and don't have enough time to even read this sentence?
Well, using Python 3, install the package with::

    git clone https://bitbucket.org/carbonizer/thekraf &&
    git -C thekraf checkout v0.1a12 &&
    pip3 install -r thekraf/requirements.txt gunicorn ./thekraf

and run the app (using gunicorn as our production server) with::

    eval "$(thekraf web gunicorn)"

Then browse to `<http://0.0.0.0:5000>`_. Run the server more securely (prevent
connections external to ``localhost``) with::

    eval "$(thekraf web --host 127.0.0.1 gunicorn)"

Then browse to `<http://127.0.0.1:5000>`_.

.. _todos:

Live Demo
---------

This site is reset regularly and it may not be up, but you may find a demo
running at `<http://tkdemo.penguinor.com>`_.

.. warning::
    The demo is running without SSL.  Do not use any sensitive information on
    the site.  Pretend as though someone is watching over your shoulder at all
    times.

TODOs
-----

- support for multiple games at once

- add a buddy system so new games can only include confirmed buddies

- add option for reload on game change or reload user's turn

- user customizable scoring (multiple preset configs already implemented)

- make shuffling players an option (shuffling is currently hard-coded to true)

- how the game ends (everyone getting one more chance)

- track game statistics

- cache certain test data so it doesn't have to be generated each time tests are
  run

.. do-not-include-after-this

Documentation
-------------

Full documentation (*under development*) can be found on `Read the Docs`_.
