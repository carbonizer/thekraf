"""
dbmngr
~~~~~~

A database manager using SQLAlchemy

The intent:
    - consolidate and simplify database configuration
    - simplify session creation
    - simplify testing

.. note::
    Currently hard-coded to use sqlite

"""
import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker


class Borg:
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state


class DatabaseManager(Borg):
    """Manage setup, configuration and session for the database

    Args:
        reset (bool): If :class:`True`, tables will be created but all records
            will be removed

    Attributes:
        path (str): Path of existing database or database to create
        engine (:class:`sqlalchemy.engine.Engine`):
        metadata (:class:`sqlalchemy.schema.MetaData`):
        Session (:class:`sqlalchemy.orm.session.sessionmaker`):
    """

    def __init__(self, path, metadata=None, reset=False):
        Borg.__init__(self)
        self.path = path
        self.engine = None
        self.Session = None
        self.metadata = metadata
        self.session_options = dict(
            autocommit=False,
            autoflush=False,
        )

        self.setup_db(reset=reset)

    def init_session(self):
        """Set up the database session"""
        # Make sure db directory exists
        if not os.path.exists(self.path):
            dirpath = os.path.dirname(self.path)
            if not os.path.exists(dirpath):
                os.makedirs(dirpath)

        if 'bind' not in self.session_options:
            url = 'sqlite:///' + self.path
            self.session_options['bind'] = create_engine(url,
                                                         convert_unicode=True)

        self.Session = scoped_session(sessionmaker(**self.session_options))
        self.engine = self.session_options['bind']

    def init_db(self, reset=False, tables=None):
        """Create tables in the database from metadata

        Args:
            reset (bool): See :meth:`__init__`
            tables (list): Passed on to
                :meth:`sqlalchemy.schema.MetaData.create_all`
        """
        # When resetting, drop tables before recreating them
        if reset:
            self.metadata.drop_all(bind=self.engine, tables=tables)
        # Create tables that don't exist
        self.metadata.create_all(bind=self.engine, tables=tables, checkfirst=True)

    def setup_db(self, reset=False):
        """Set up db session and possibly init

        Args:
            reset(bool):
        """

        self.init_session()

        if self.metadata:
            self.init_db(reset=reset)
        else:
            assert not reset, "Resetting db requires metadata"
