from __future__ import print_function
import os
import shutil
import sys
import tempfile
from dbmngr import DatabaseManager
from thekraf import metadata


class TmpMixin(object):
    tmp_root = ''
    metadata = metadata

    @classmethod
    def setup_tmp_root(cls, prefix='tmp-test-root'):
        """Create a directory for testing use

        :param prefix: (passed to :func:`tempfile.mkstemp`)
        :type prefix: str
        """
        cls.tmp_root = tempfile.mkdtemp(prefix=prefix)

    @classmethod
    def teardown_tmp_root(cls):
        shutil.rmtree(cls.tmp_root)

    def setup_tmp_db(self, prefix='tmp-test-db'):
        """Set up a temporary database for unit testing purposes

        Args:
            prefix (str): The prefix of the db file to create
                (passed to :func:`tempfile.mkstemp`)
        """
        db_fd, db_path = tempfile.mkstemp('.db', prefix, getattr(self, 'tmp_root', None))
        sys.stderr.write(db_path + '\n')
        os.close(db_fd)
        self.dbm = DatabaseManager(db_path, metadata)
        self.sess = self.dbm.Session()

    def teardown_tmp_db(self):
        # self.dbm.sess.close_all()
        os.unlink(self.dbm.path)
