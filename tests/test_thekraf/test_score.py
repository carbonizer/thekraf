import copy
from nose.tools import eq_
import thekraf.config as cfg
import thekraf.db.models as mdls
from thekraf.diceutils import DiceUtils
from thekraf.score import Score


class TestScore(object):
    opts = None

    @classmethod
    def setup_class(cls):
        d = cfg.load_config_item('score_options',
                                 select=lambda x: x['name'] == 'default')[0]
        cls.opts = mdls.ScoreOptions(**d)

    def test_calc_of_a_kind(self):
        for n, scheme, scores in (
                (3, None, (
                        1000, 200, 300, 400, 500, 600
                )),
                (5, mdls.ScoreOptions.ADD_SCHEME, (
                        3000, 600, 900, 1200, 1500, 1800
                )),
                (5, mdls.ScoreOptions.DOUBLE_SCHEME, (
                        4000, 800, 1200, 1600, 2000, 2400
                )),
        ):
            for val, score in enumerate(scores, 1):
                yield self.check_calc_of_a_kind, score, n, val, scheme

    def check_calc_of_a_kind(self, score, n, val, scheme):
        my_opts = copy.deepcopy(self.opts)
        my_opts.fourplusscheme = scheme

        eq_(score, Score.calc_of_a_kind(my_opts, n, val))

    def test_score_all(self):
        opts = self.opts
        # Here, the pairs are score/expected_value then dice/input because the
        # expected value tends to be consistently short and the input varies in
        # length significantly
        score_dice_pairs = [
            # 6 of any number
            (opts.kind6, [5 for _ in range(6)]),
            # Two triplets
            (opts.twotriplets, [1, 1, 1, 5, 5, 5]),
            # Four of any number with a pair
            # (this is equivalent to "Three Pairs")
            (opts.kind4 + (2 * opts.single5), [c for c in '111155']),
            # Three pairs
            (opts.threepair, [c for c in '115533']),
            # 1-6 straight
            (opts.straight, '135246'),
        ]
        # Now, I switched the order (in the long string below) to input:output
        # since it tends to be easier to read as input -> output
        score_dice_pairs.extend((int(score), tuple(map(int, dice)))
                                for dice, score in (a.split(':') for a in '''
        122255:400
        5:50 1:100 345:0
        111:1000 222:200 345:0 515:200 555:500
        4433:0 5511:300 4444:2000 1111:2000 5555:2000
        14433:0 14444:2100 33333:4000 55555:4000
        '''.strip().split()))
        for score, dice in score_dice_pairs:
            yield self.check_score_all, score, dice

    def check_score_all(self, score, dice):
        eq_(score, Score.score_all(self.opts, dice))

    def test_subscores(self):
        dice_subscores_pairs = (
            (
                DiceUtils.create_dice(dice_str),
                {a: Score.score_all(self.opts, DiceUtils.create_dice(a))
                 for a in substrs.split()}
            )
            for dice_str, substrs in (
                (chunk.strip() for chunk in line.split(':'))
                for line in '''
                    11:11 1
                    112: 11 1
                    1152: 115 15 11 1 5
                    '''.strip().splitlines()
            )
        )
        for dice, subscores in dice_subscores_pairs:
            yield self.check_subscores, dice, subscores

    def check_subscores(self, dice, subscores):
        eq_(subscores, Score.subscores(self.opts, dice))
