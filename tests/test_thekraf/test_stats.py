from decimal import Decimal
from nose.tools import eq_
import thekraf.config as cfg
import thekraf.db.models as mdls
from thekraf.score import Score
from thekraf.stats import expected_value, precache_evs


class TestStats(object):
    opts = None

    @classmethod
    def setup_class(cls):
        d = cfg.load_config_item('score_options',
                                 select=lambda x:
                                     x['name'] == 'ev_paper')[0]

        cls.opts = mdls.ScoreOptions(**d)
        Score.precache_scores(opts=cls.opts)
        cls.opts.ev_cache.update(precache_evs(cls.opts))

    def test_expected_value(self):
        for ev, pts, n in (
                ('548.858', 0, 6),
                (250, 250, 2),
                ('274.669', 250, 1),
                ('1249.464', 800, 6),
                (1050, 1050, 4),
        ):
            yield self.check_expected_value, Decimal(ev), Decimal(pts), n

    def check_expected_value(self, ev, pts, n):
        eq_(ev, expected_value(self.opts, pts=pts, n=n,
                               cache=self.opts.ev_cache).quantize(
            Decimal('1.000')))
