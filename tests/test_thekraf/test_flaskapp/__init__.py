import flask
from thekraf.db.mapper import config_sqla_models
import thekraf.db.models as mdls
from thekraf.flaskapp.app import create_and_config_app
from thekraf.utils import get_subclasses
from tests import TmpMixin


class TestBase(TmpMixin):
    def sync_session(self, client):
        with client.session_transaction() as flask_sess:
            flask_sess.clear()
            flask_sess.update(flask.session)

    def setup(self):
        self.setup_tmp_root()
        self.setup_tmp_db()
        app = create_and_config_app()
        app.config['TESTING'] = True
        # Ignore crsf while tesing
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = self.dbm.engine.url
        config_sqla_models(get_subclasses(mdls.BaseModel), self.dbm)
        self.app = app

        # Set up users to test with
        with self.app.test_client() as c:
            ds = self.app.extensions['security'].datastore
            """:type : flask_security.SQLAlchemyUserDatastore"""
            user_role = ds.create_role(name='user', description='user')
            ds.db.session.commit()
            for n in range(1, 3):
                name = 'user{}'.format(n)
                user = ds.create_user(email='{}@example.com'.format(name),
                                      first=name.capitalize(),
                                      last='Mc' + name.capitalize(),
                                      password='password',
                                      username=name)
                ds.add_role_to_user(user, user_role)
                ds.db.session.commit()

    def teardown(self):
        self.teardown_tmp_db()
        self.teardown_tmp_root()
