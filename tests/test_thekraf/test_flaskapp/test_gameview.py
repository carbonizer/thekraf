import flask
from flask_security.utils import login_user
from nose.tools import eq_, assert_not_equal, assert_not_in, assert_in
from tests.test_thekraf.test_flaskapp import TestBase
import thekraf.db.models as mdls
from thekraf.diceutils import DiceUtils
from thekraf.flaskapp.gameview import GameView
# from thekraf.flaskapp.newgameview import NewGameView


class TestGameView(TestBase):

    def setup(self):
        super(TestGameView, self).setup()

    def test_post_score(self):
        for a in (True, False):
            yield self.check_post_score, a

    def check_post_score(self, is_anonymous):
        with self.app.test_client() as c:
            # Get dummy route to establish the session
            c.get('/hello')

            # If running test as a registered user, log user in
            if not is_anonymous:
                ds = self.app.extensions['security'].datastore
                user = ds.get_user('user1@example.com')
                login_user(user)
                self.sync_session(c)

            # Start with a fresh, non-busted roll
            game = None
            for _ in range(10):
                rv = c.get('/resetgame')
                game = GameView.load_game()
                if not game.turn.busted:
                    break

            # Get a set of scoring dice
            dice_str, score = list(game.turn.subscores.items())[0]

            # Configure post data to indicate selecting those dice
            d = {'dice_select-{}'.format(i): game.turn.rolled[i]
                 for i in DiceUtils.indices_of(game.turn.rolled, dice_str)}
            # Form submitted with score button
            d['score_button'] = 'True'

            # Post form data
            rv = c.post('/game', data=d)

            rolled = game.turn.rolled
            game = GameView.load_game()

            # Score should be what we just scored
            eq_(score, game.turn.score)
            # Score should not cause roll
            eq_(rolled, game.turn.rolled)

    def test_muliplayer(self):
        for a in (False,):
            yield self.check_multiplayer, a

    def check_multiplayer(self, is_anonymous):
        game_id = -1
        with self.app.test_client() as c:
            # Get dummy route to establish the session
            c.get('/hello')

            # If running test as a registered user, log user in
            if not is_anonymous:
                ds = self.app.extensions['security'].datastore
                user = ds.get_user('user1@example.com')
                login_user(user)
                self.sync_session(c)

            # Form submitted with reset button
            d = {
                'reset_button': 'True',
                'user_select-0': 'user1',
                'user_select-1': 'user2',
                'mode_select': 'points',
            }

            # Post form data
            rv = c.post('/newgame', data=d)

            game = GameView.load_game()
            game_id = game.id
            eq_(1, len(user.games))
            eq_(2, len(game.users))

        with self.app.test_client() as c:
            c.get('/hello')

            if not is_anonymous:
                ds = self.app.extensions['security'].datastore
                user = ds.get_user('user2@example.com')
                login_user(user)
                self.sync_session(c)

            rv = c.get('/game', data=d)
            game = GameView.load_game()

            assert_in('game_id', flask.session)
            eq_(game_id, game.id)

    def teardown(self):
        super(TestGameView, self).teardown()
