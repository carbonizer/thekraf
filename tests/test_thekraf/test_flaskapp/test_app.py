from nose.tools import eq_
from tests import TmpMixin
from thekraf.flaskapp.app import create_and_config_hello_app
from thekraf.db.mapper import config_sqla_models
from thekraf.utils import get_subclasses
import thekraf.db.models as mdls


class TestBase(TmpMixin):

    def setup(self):
        app = create_and_config_hello_app()
        self.setup_tmp_root()
        self.setup_tmp_db()
        config_sqla_models(get_subclasses(mdls.BaseModel), self.dbm)
        app.config['TESTING'] = True
        self.app = app.test_client()

    def test_hello(self):
        rv = self.app.get('/hello')
        eq_(rv.data.decode(), 'Hello World!')

    def teardown(self):
        self.teardown_tmp_db()
        self.teardown_tmp_root()


class TestUser(TmpMixin):
    def setup(self):
        app = create_and_config_hello_app()
        self.setup_tmp_root()
        self.setup_tmp_db()

        app.config['TESTING'] = True
        self.app = app.test_client()

    def test_blah(self):
        email = 'user@example.com'
        self.sess.add(mdls.User(email=email))
        self.sess.commit()
        users = self.sess.query(mdls.User).all()
        eq_(1, len(users))
        eq_(email, users[0].email)

    def teardown(self):
        self.teardown_tmp_db()
        self.teardown_tmp_root()
