from nose.tools import eq_, ok_
import thekraf.config as cfg
import thekraf.db.models as mdls
from thekraf.game import Game
from thekraf.gamemodels import Turn
from thekraf.score import ScoreCache


class TestTurn(object):
    opts = None

    @classmethod
    def setup_class(cls):
        d = cfg.load_config_item('score_options',
                                 select=lambda x: x['name'] == 'default')[0]
        cls.opts = mdls.ScoreOptions(**d)
        ScoreCache.precache_scores(cls.opts)

    def test__init__(self):
        turn = Turn(dice_score_pairs=([[1, 2, 3], 0],))
        eq_(turn.dice_score_pairs, [((1, 2, 3), 0)])

    def test_select_and_score(self):
        game = None
        n_attempts = 10
        for i in range(n_attempts):
            game = Game(opts=self.opts)
            if game.turn.busted:
                game = Game(opts=self.opts)
            else:
                break
        if game.turn.busted:
            raise AssertionError(
                'Highly unlikely that fresh roll busted {} times'.format(
                    n_attempts))
