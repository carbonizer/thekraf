import shlex
from nose.tools import assert_equal, eq_
from thekraf.__main__ import parse_args
from thekraf import load_game_opts
import thekraf.config as cfg
from thekraf.score import ScoreCache


def test_parse_args():
    assert_equal([1, 2], parse_args(shlex.split('score 1 2')).dice)
    assert_equal([3, 3, 5, 6], parse_args(shlex.split('score 33,65')).dice)


def test_score_cmd():
    for score, dice_str in (
            (6000, 6 * '5'),
            (0, '553'),
            (50, '5'),
    ):
        yield check_score, score, dice_str


def check_score(score, dice_str):
    pargs = parse_args(['score', dice_str])
    if not cfg.GAME_OPTS:
        cfg.GAME_OPTS.update(
            load_game_opts(data=pargs.config_file.data,
                           config_name=pargs.config))
    eq_(score, pargs.func(pargs))
