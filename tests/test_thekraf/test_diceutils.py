from nose.tools import eq_
from thekraf.diceutils import DiceUtils


class TestDice(object):

    def test_create_dice(self):
        str_repr_pairs = [
            ('111', '111'),
            ('123', '1 2 3'),
            ('123', '1; 2   : 3'),
            ('435', [4, 3, 5]),
            ('645', ['6', '4', '5']),
        ]
        for dice_str, dice_repr in str_repr_pairs:
            yield self.check_create_dice, tuple(map(int, dice_str)), dice_repr

    @staticmethod
    def check_create_dice(expected_dice, dice_repr):
        eq_(expected_dice, DiceUtils.create_dice(dice_repr))
