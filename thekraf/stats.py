"""
thekraf.flaskapp.statsview
==========================

Logic related to calculating expected value tables
"""
from decimal import Decimal
from math import factorial
import thekraf.config as cfg
from thekraf.diceutils import DiceUtils


def product(iterable, start=1):
    """Like `sum()` but with multiplication"""
    result = start
    for item in iterable:
        result *= item
    return result


def combination(n, k, replacement=False):
    """n choose k combination with option for replacement

    Examples:
        >>> combination(52, 5)
        2598960
        >>> combination(4, 3, True)
        20
        >>> combination(6, 3, True)
        56

    Args:
        n (int): Number of items
        k (int): Number of items to choose
        replacement (bool): True if items are reusable

    Returns:
        int: Number of combinations
    """
    if replacement:
        n = n + k - 1
    return factorial(n) // (factorial(k) * factorial(n - k))


ev_caches = {}


def precache_evs(opts):
    """Build ev cache for scoring options

    Args:
        opts (thekraf.config.ScoreOptions): Score options

    Returns:
        dict[tuple[int, int], Decimal]: EV cache
    """
    cache = {}
    for pts in range(0, opts.points, opts.single5):
        for n in range(1, cfg.GAME_DICE_MAX_COUNT + 1):
            k = (pts, n)
            if k not in cache:
                ev = expected_value(opts, *k, cache=cache)
                cache[k] = ev
    return cache


def expected_value(opts, pts, n, cache=None):
    """Return the greater of the expected value of rolling n dice, or points

    After rolling, there is frequently more than one way to score the dice.  For
    example, [1, 3, 5] the player can score the 1, the 5, or both.  After
    calculating the ev for each option, the best option results in the maximum
    of these values.

    The ev for n random dice can be found by summing the best ev for each
    permutation of dice and dividing the sum by the total number of
    permutations.

    Args:
        pts (Decimal): Points accumulate thus far in a turn
        n (int): Number of dice available to reroll
        opts (thekraf.config.ScoreOptions): Score options
        cache (dict[tuple[int, int], Decimal]): EV cache

    Returns:
        Decimal: Greater of expected value or points
    """
    pts = Decimal(pts)
    # Return points if the goal has already been reached
    if pts >= opts.points:
        return pts

    # Create a cache if one has not been provided
    if cache is None:
        cache = {}

    # For combos dice of len n that have subscores...  We can skip the other
    # combos because their evs would always be 0 anyway.
    bests = []
    for all_dice_str, subscores in opts.score_any_by_num_cache[n].items():
        # Calculate each ev, depending on how the dice are scored
        evs = []
        for dice_str, score in subscores.items():
            # Dice remaining to reroll
            next_n = n - len(dice_str)
            # Roll a whole new set of dice, if all dice were used to score
            if not next_n:
                next_n = cfg.GAME_DICE_MAX_COUNT
            next_pts = pts + Decimal(score)

            # Use cached ev if it exists for these args
            k = (int(next_pts), next_n)
            ev = cache.get(k)
            # Calculate ev and add to cache
            if ev is None:
                ev = expected_value(opts, *k, cache=cache)
                if k[0] <= opts.points:
                    cache[k] = ev

            evs.append(ev)

        # Keep the max ev as our best
        best_ev = max(evs)
        # Unique permutations of this combo of dice
        perms = Decimal(factorial(len(all_dice_str))/product(
            factorial(all_dice_str.count(c)) for c in set(all_dice_str)
        ))
        bests.append(perms * best_ev)

    # The ev for n random dice
    ev_roll = (sum(bests, Decimal()) /
               Decimal(len(DiceUtils.VALS) ** len(all_dice_str)))

    # Return the greater of the ev from rerolling or the points already obtained
    return max(ev_roll, pts)


def formatted_ev_cache(opts):
    """Return the ev cache as a table of representative strings

    Ensure that ints don't have decimals.  Decimals are rounded to three places.
    Impossible pts/n combos will be represented as '-'.

    Args:
        opts (thekraf.config.ScoreOptions): Score options

    Returns:
        list[list[str]]: EV cache as table of strings
    """
    zipped = zip(*opts.ev_cache.keys())
    ptss, ns = (sorted(set(z)) for z in zipped)

    # String of int or Decimal
    data = [[str(pts if pts == opts.ev_cache.get((pts, n))
                 else opts.ev_cache.get((pts, n)).quantize(Decimal('1.000')))
             for n in ns] for pts in ptss]

    impossibles = []
    # From 0 points to scoring the minimum for 2 full set of dice
    for pts in range(0, 2 * cfg.GAME_DICE_MAX_COUNT * opts.single5,
                     opts.single5):
        for n in range(1, cfg.GAME_DICE_MAX_COUNT + 1):
            # Try to match points by adding combinations of 1s and 5s
            # (number of 1s plus number of 5s must equal the number of dice
            # scored so far).  If it cannot be done, it is impossible.
            if all(not any(a * opts.single5 + b * opts.single1 == pts
                           # The sum of an element in range and the element
                           # of the same index in the reverse of the range
                           # is constant (in this case, the number of scored
                           # dice).
                           for a, b in zip(range(scored + 1),
                                           reversed(range(scored + 1))))
                   # Scored dice is a multiple of a full set of dice less
                   # the number remaining to roll
                   for scored in (m * cfg.GAME_DICE_MAX_COUNT - n
                                  for m in range(1, 3))):
                impossibles.append((pts, n))

    # Update representation for impossible combos
    for pts, n in impossibles:
        data[pts // opts.single5][n - 1] = '-'

    # Add header col/row description
    headers = ['Points/Dice to Role']
    headers.extend(str(n) for n in range(1, cfg.GAME_DICE_MAX_COUNT + 1))
    header_col = list(range(0, opts.points + opts.single5, opts.single5))
    rows = [headers]
    rows.extend([header] + r for header, r in zip(header_col, data))

    return rows
