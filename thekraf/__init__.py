from dbmngr import DatabaseManager
from multilogr import BaseMultilogr
import thekraf.config as cfg
from thekraf import config as cfg
from thekraf.db import models as mdls
from thekraf.db.models import BaseModel
from thekraf.utils import get_subclasses

__version__ = '0.1a12'


class Multilogr(BaseMultilogr):
    LOGS_SPEC = cfg.LOGS_SPEC


from thekraf.db.models import BaseModel
from thekraf.db.mapper import config_metadata, config_sqla_models
from thekraf.utils import get_subclasses

metadata = config_metadata()

dbm = DatabaseManager(cfg.DB_PATH, metadata)
config_sqla_models(get_subclasses(BaseModel), dbm)


def load_game_opts():
    sess = dbm.Session()

    # Create a dict of game opts
    d = {opts.name: opts for opts in sess.query(mdls.ScoreOptions).all()}

    cfg.GAME_OPTS.clear()
    cfg.GAME_OPTS.update(d)
