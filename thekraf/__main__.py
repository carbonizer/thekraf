"""
thekraf.__main__
================

The main entry-point for the package
"""
import argparse
import sys
import timeit
from pprint import pprint

from flask_security.utils import encrypt_password

import thekraf.config as cfg
import thekraf.db.models as mdls
import thekraf.diceutils as diceutils
import thekraf.score
import thekraf.stats
from thekraf import Multilogr
from thekraf import __version__, dbm, load_game_opts
from thekraf.flaskapp.__main__ import config_web_parser, web_cmd

logr = Multilogr.get_logr('thk')


# Reference directly so it isn't flagged as an unused import
if web_cmd:
    pass


class ConfigFileType(object):
    """A type to use with argparse that loads config data

    Attributes:
        path (str): Path of the config file.  This is also the argument
            received from argparse.
        data (dict): The data loaded from the file.
    """

    def __init__(self, path, data=None):
        self.path = path
        if data is None:
            try:
                self.data = cfg.load_config_file(self.path)
            except Exception as e:
                raise argparse.ArgumentTypeError(repr(e))
        else:
            self.data = data


class StoreDigits(argparse.Action):
    def __init__(self, option_strings, dest, **kwargs):
        super(StoreDigits, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        vals = getattr(namespace, self.dest, [])
        if vals is None:
            vals = []
        vals = sorted(vals +
                      list(diceutils.DiceUtils.create_dice(' '.join(values))))
        setattr(namespace, self.dest, vals)


def parse_args(args):
    """Parse program arguments

    The program comprises multiple commands.  Each command has its own
    entry-point and sub-parser.

    The entry-point function is named ``<command>_cmd``, and it takes the
    parsed argument namespace ``pargs`` as its sole argument.  If it has a
    return value, it is a string that will be printed to ``stdout``.

    The sub-parser config function is named ``config_<command>_parser``, and it
    takes the :class:`argparse.ArgumentParser` ``parser`` that it configures as
    its only argument.  At minimum, the function must set the entry-point
    function with an expression of the form::

        parser.set_defaults(func=<command_func>, ...)

    Args:
        args (tuple[str]): Arguments to parse

    Returns:
        argparse.Namespace: Parsed namespace

    """
    parser = argparse.ArgumentParser()

    # Any global arguments would be added here
    parser.add_argument('--version', action='version',
                        version='{} {}'.format(cfg.PKG_NAME, __version__))
    parser.add_argument('--config', '-c',
                        default='default', metavar='NAME',
                        help='Name of config to use')

    parser.add_argument('--config-file', '--configfile', '--cfile',
                        type=ConfigFileType,
                        default=ConfigFileType(cfg.PKG_CONFIGS_DEFAULT_PATH,
                                               cfg.DEFAULTS),
                        metavar='FILE',
                        help='Path to config file to use')

    # Configure subparsers
    subparsers = parser.add_subparsers(title='commands')
    for config_func in (
            config_score_parser,
            config_cache_parser,
            config_reset_parser,
            config_timeit_parser,
            config_web_parser,
    ):
        name = config_func.__name__.split('_', 2)[1]

        # Help string is the first line of the doc string of the cmd func
        cmd_func = globals().get(name + '_cmd')
        help_str = cmd_func.__doc__.splitlines()[0]

        config_func(subparsers.add_parser(name, help=help_str))

    # Placeholder func argument
    parser.set_defaults(func=None)

    pargs = parser.parse_args(args)

    # If no command has been specified, print help
    if not pargs.func:
        parser.parse_args(['-h'])

    return pargs


def config_score_parser(parser):
    parser.add_argument('dice', nargs='+', action=StoreDigits)
    parser.set_defaults(func=score_cmd)


def score_cmd(pargs):
    """Score dice representation"""
    return thekraf.score.Score.score_all(cfg.GAME_OPTS['default'], pargs.dice)


def config_cache_parser(parser):
    parser.add_argument('--path', '-p', help='Out path for cache')
    parser.add_argument('type', choices=['score', 'ev'],
                        help='Type of cache to build')
    parser.set_defaults(func=cache_cmd)


def cache_cmd(pargs):
    """Cache scores"""
    if pargs.type == 'score':
        thekraf.score.Score.precache_scores()
        if pargs.path:
            thekraf.score.Score.save_score_caches(pargs.path)
    elif pargs.type == 'ev':
        opts = cfg.EvPaperOptions
        thekraf.score.Score.precache_scores(opts=opts)
        cache = thekraf.stats.precache_evs(opts)
        rows = thekraf.stats.formatted_ev_cache(cache, opts=opts)

        pprint(rows)
    return 'success'


def config_timeit_parser(parser):
    parser.add_argument('names', nargs='+',
                        choices=['cache', 'precache-score',
                                 'precache-ev', 'calc-score'])
    parser.set_defaults(func=timeit_cmd)


def timeit_cmd(pargs):
    """Timing analysis of some commands"""
    results = {}
    for name in pargs.names:
        d = {}
        if name == 'precache-score':
            d['stmt'] = '\n'.join([
                'for dice in cache.keys(): cache[dice]',
            ])
            d['setup'] = '\n'.join([
                'import thekraf.diceutils as diceutils',
                'cache = diceutils.precache_scores()',
            ])
            d['number'] = 10
        elif name == 'precache-ev':
            d['stmt'] = '\n'.join([
                'cache = thekraf.stats.precache_evs(opts)',
            ])
            d['setup'] = '\n'.join([
                'import thekraf.config as cfg',
                'import thekraf.score',
                'import thekraf.stats',
                'opts = cfg.EvPaperOptions',
                'thekraf.score.Score.precache_scores(opts=opts)',
            ])
            d['number'] = 1
        elif name == 'calc-score':
            d['stmt'] = '\n'.join([
                'for dice in cache.keys(): diceutils.calc_score(dice)',
            ])
            d['setup'] = '\n'.join([
                'import thekraf.diceutils as diceutils',
                'cache = diceutils.precache_scores()',
            ])
            d['number'] = 10
        elif name == 'cache':
            d['stmt'] = '\n'.join([
                'diceutils.precache_scores()',
            ])
            d['setup'] = '\n'.join([
                'import thekraf.diceutils as diceutils',
            ])
            d['number'] = 10

        result = timeit.timeit(**d)
        results[name] = '{} ({} times)'.format(result, d['number'])
    return repr(results)


def config_reset_parser(parser):
    parser.add_argument('tables', nargs='+',
                        choices=['full', 'games', 'score_options', 'users'],
                        help='Tables to reset')
    parser.set_defaults(func=reset_cmd)


def reset_cmd(pargs):
    """Reset data in database"""
    if 'full' in pargs.tables:
        logr.info('Dropping all tables from db and recreating...')
        dbm.init_db(reset=True)
        pargs.tables.pop(pargs.tables.index('full'))
    for name in pargs.tables:
        if name == 'score_options':
            opts_data = []
            # If a config file was given and it has data in it, use that
            if pargs.config_file:
                data = pargs.config_file.data
                opts_data = data.get(pargs.config, {}).get('score_options', [])

            add_score_options(opts_data, reset=True)
        elif name == 'users':
            users = []
            # If a config file was given and it has user data in it, use that
            # data as new user data
            if pargs.config_file:
                data = pargs.config_file.data
                users = data.get(pargs.config, {}).get('users', [])

            reset_users(users)
        else:
            sess = dbm.Session()
            table = dbm.metadata.tables[name]
            sess.execute(table.delete())
            sess.commit()


def reset_users(new_user_data=()):
    """Reset users table in database

    Args:
        new_user_data (tuple[dict]): Users to add to table after clear
    """
    # Clear the table
    sess = dbm.Session()
    sess.execute(dbm.metadata.tables['users'].delete())
    sess.commit()
    logr.info('Emptied users table')

    # Add new users based on data provided
    if new_user_data:
        from thekraf.flaskapp.app import create_and_config_hello_app
        app = create_and_config_hello_app()
        ds = app.extensions['security'].datastore
        app.config['TESTING'] = True
        with app.test_client() as c:
            rv = c.get('/hello')
            for info in new_user_data:
                d = dict(info)
                d.setdefault('password', encrypt_password('password'))
                user = ds.create_user(**d)
                logr.info('Added user {}'.format(repr(user)))

            ds.db.session.commit()


def add_score_options(new_opts_data, reset=False):
    """Add score options entries to table in database

    Args:
        new_opts_data (tuple[dict]): Users to add to table after clear
        reset (bool): Set to `True` to clear table before adding entries
    """
    tablename = mdls.ScoreOptions.__tablename__
    sess = dbm.Session()
    # Clear the table
    if reset:
        sess.execute(dbm.metadata.tables[tablename].delete())
        sess.commit()
        logr.info('Emptied {} table'.format(repr(tablename)))

    # Add new opts based on data provided
    for d in new_opts_data:
        logr.info('Caching {}'.format(repr(d['name'])))
        opts = mdls.ScoreOptions(**d)
        thekraf.score.Score.precache_scores(opts)
        opts.ev_cache.update(thekraf.stats.precache_evs(opts))
        sess.add(opts)
        sess.commit()
        logr.info('Added {} to db'.format(repr(opts.name)))


def update_db_score_options(**kwargs):
    """Verify all score option config have been loaded into the database

    Args:
        **kwargs (dict): Keyword args passed to func:`cfg.load_config_item()`
    """
    # Load score options config
    from_config_opts = cfg.load_config_item('score_options', **kwargs)

    # Load related name from db
    sess = dbm.Session()
    known_opts_names = [name for name, in
                        sess.query(mdls.ScoreOptions.name).all()]

    # If anything from the config is missing from the db, add them
    missing = tuple(opts for opts in from_config_opts if
                    not any(opts['name'] == name for name in known_opts_names))
    if missing:
        logr.info('Score options {!r} are missing from db.  Adding...'.format(
            [opts['name'] for opts in missing]))
        add_score_options(missing)


def main(args=sys.argv[1:]):
    pargs = parse_args(args)
    logr.info('Starting thekraf')
    if pargs.func != reset_cmd:
        # Update db from config as needed
        update_db_score_options(data=pargs.config_file.data,
                                config_name=pargs.config)
        # Load from db
        load_game_opts()
    result = pargs.func(pargs)
    print(result)
    logr.info('thekraf finished successfully')


if __name__ == '__main__':
    main()
