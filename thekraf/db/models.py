"""
thekraf.db.models
~~~~~~~~~~~~~~~~~

Models for the database
"""
from flask_security import RoleMixin, UserMixin


class BaseModel(object):
    __tablename__ = 'noname'
    query = None


class Role(BaseModel, RoleMixin):
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', '(noname_role')
        self.description = kwargs.get('description', '(no_description)')


class User(BaseModel, UserMixin):
    def __init__(self, **kwargs):
        self.email = kwargs.get('email', 'unknown@example.com')
        self.username = kwargs.get('username', self.email)
        self.password = kwargs.get('password', '')
        self.active = kwargs.get('active', False)
        self.first = kwargs.get('first', '(first)')
        self.last = kwargs.get('last', '(last)')
        self.nickname = kwargs.get('nickname', self.username)

    def __repr__(self):
        strng = ', '.join('{}: {}'.format(name, repr(getattr(self, name)))
                          for name in ('username', 'email',
                                       'nickname', 'name'))
        return 'User<{}>'.format(strng.split(': ', 1)[1])

    @property
    def name(self):
        return ' '.join((self.first, self.last))


class ScoreOptions(BaseModel):
    """Configurable scoring options

    Attributes:
        name (str): Name of these options
        description (str): Description of these options
        single1 (int): A single die of value 1
        single5 (int): A single die of value 5
        triple1 (int): A triplet of dice of value 1 (usually 300 or 1000)
        fourplusscheme (str): Scoring scheme for 4-6 of a kind
        kind4 (int): Score for 4 of a kind when using the 'set' scheme
        kind5 (int): Score for 5 of a kind when using the 'set' scheme
        kind6 (int): Score for 6 of a kind when using the 'set' scheme
        straight (int): Score for a straight (123456)
        threepair (int): Score for three pairs (e.g. 112233)
        twotriplets (int): Score of two triplets (e.g. 111222) (instead of
            scoring each triplet on its own)
        fullhousebonus (int): Score a triplet and pair together as the triplet
            score plus this bonus (instead of scoring the pair on its own)
        kind4bonus (int): Score a 4 of a kind and a pairs together as the 4 of a
            kind plus this bonus (instead of scoring the pair on its own)
        points (int): Goal in points-based mode
        score_cache (dict[str, int]): dice_str -> score, if all dice are scoring
        score_any_cache (dict[str, dict[str, int]]): dice_str -> sub-dict of
            `score_cache`. The sub-keys will be sub-combos of dice_str in
            `score_cache` (meaning the sub-combo comprises all scoring dice.
        score_any_by_num_cache (dict[int, dict[str, dict[str, int]]]): The
            `score_any_cache` organized by the number of dice.
        ev_cache (dict[tuple[int, int], Decimal]): (pts, n) -> related ev
    """
    ADD_SCHEME = 'add'
    """str: Add triplet value for each die more than 3

    Example:
        3 of a kind -> 1000, 4 -> 2000, 5 -> 3000, 6 -> 4000
    """
    DOUBLE_SCHEME = 'double'
    """str: Double triplet value for each die more than 3

    Example:
        3 of a kind -> 1000, 4 -> 2000, 5 -> 4000, 6 -> 8000
    """
    SET_SCHEME = 'set'
    """str: Use set values (kind4, kind5, kind6) regardless of die value"""

    DEFAULTS = dict(
        name='(unnamed)',
        description='',
        single1=100,
        single5=50,
        triple1=1000,
        fourplusscheme=ADD_SCHEME,
        kind4=2000,
        kind5=4000,
        kind6=6000,
        threepair=1500,
        straight=2500,
        points=10000,
    )

    def __init__(self, **kwargs):
        def _get_kwarg_or_default(name):
            return kwargs.get(name, self.DEFAULTS.get(name))

        self.name = _get_kwarg_or_default('name')
        self.description = _get_kwarg_or_default('description')
        self.single1 = _get_kwarg_or_default('single1')
        self.single5 = _get_kwarg_or_default('single5')
        self.triple1 = _get_kwarg_or_default('triple1')
        self.fourplusscheme = _get_kwarg_or_default('fourplusscheme')
        self.kind4 = _get_kwarg_or_default('kind4')
        self.kind5 = _get_kwarg_or_default('kind5')
        self.kind6 = _get_kwarg_or_default('kind6')
        self.threepair = _get_kwarg_or_default('threepair')
        self.straight = _get_kwarg_or_default('straight')
        self.twotriplets = _get_kwarg_or_default('twotriplets')
        self.kind4bonus = _get_kwarg_or_default('kind4bonus')
        self.fullhousebonus = _get_kwarg_or_default('fullhousebonus')
        self.points = _get_kwarg_or_default('points')
        self.score_cache = kwargs.get('score_cache', {})
        self.score_any_cache = kwargs.get('score_any_cache', {})
        self.score_any_by_num_cache = kwargs.get('score_any_by_num_cache', {})
        self.ev_cache = kwargs.get('ev_cache', {})

        schemes = (self.ADD_SCHEME, self.DOUBLE_SCHEME, self.SET_SCHEME)
        if self.fourplusscheme not in schemes:
            ValueError('Scheme is {}, not one of {}'.format(
                *map(repr, (self.fourplusscheme, schemes))))

        for key in kwargs.keys():
            if key not in self.__dict__.keys():
                KeyError('Unknown keyword argument {}'.format(repr(key)))


mdls = (
    Role,
    User,
)
