"""
thekraf.db.listeners
====================

A collection of SQLAlchemy event listeners to be applied to mapped models

Note:
    SQLAlchemy will ignore fields that don't exist on the target
"""
from datetime import datetime


def created_modified(mapper, connection, target):
    """Sets the created/modified fields"""
    now = datetime.utcnow()
    target.created = now
    target.modified = now


def modified(mapper, connection, target):
    """Sets the modified field"""
    target.modified = datetime.utcnow()
