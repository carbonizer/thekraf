"""
thekraf.db.mapper
=================

Object-relational mapping of models for SQLAlchemy
"""
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer,\
    MetaData, PickleType, String, Table, event
from sqlalchemy.orm import backref, mapper, relationship
from thekraf.game import Game
import thekraf.db.listeners as evtls
import thekraf.db.models as mdls
from thekraf.utils import get_subclasses, MutableList


def config_sqla_models(models, dbm):
    """Configure basic models for SQLAlchemy

    Note:
        This can only be called after the metadata and the database manager
        have been configured.

    Args:
        models (tuple[BaseModel]): Models to config
        dbm (DatabaseManager): The database manager providing the session
            factory

    """

    for model in models:
        # Add the query property to each model.  Certain libraries require this
        # to be present.
        model.query = dbm.Session.query_property()

        # Add event listeners to indicated classes
        if model in (Game, mdls.Role, mdls.User):
            event.listen(model, 'before_insert', evtls.created_modified)
            event.listen(model, 'before_update', evtls.modified)


def config_metadata():
    """Configure the metadata for the ORM

    This metadata may then be used to configure the database manager

    Returns:
        MetaData: The configured metadata

    """

    metadata = MetaData()

    # Set the default table name for the models
    for model in get_subclasses(mdls.BaseModel):
        model.__tablename__ = model.__name__.lower() + 's'

    # Default table names may be overridden here as needed
    mdls.ScoreOptions.__tablename__ = 'score_options'

    mapper(Game, Table(
        Game.__tablename__, metadata,
        Column('id', Integer, primary_key=True),
        Column('created', DateTime),
        Column('modified', DateTime),
        Column('is_anonymous', Boolean),
        Column('mode', String(16)),
        Column('goal', Integer),
        Column('min_first_bank', Integer),
        Column('min_bank', Integer),
        Column('cycles', MutableList.as_mutable(PickleType)),
        Column('opts', PickleType),
    ))

    mapper(mdls.Role, Table(
        mdls.Role.__tablename__, metadata,
        Column('id', Integer, primary_key=True),
        Column('created', DateTime),
        Column('modified', DateTime),
        Column('name', String(64), unique=True),
        Column('description', String(255)),
    ))

    mapper(mdls.ScoreOptions, Table(
        mdls.ScoreOptions.__tablename__, metadata,
        Column('id', Integer, primary_key=True),
        Column('created', DateTime),
        Column('modified', DateTime),
        Column('name', String(64), unique=True),
        Column('description', String(255)),
        Column('points', Integer),
        Column('fourplusscheme', String),
        Column('single1', Integer),
        Column('single5', Integer),
        Column('triple1', Integer),
        Column('kind4', Integer),
        Column('kind5', Integer),
        Column('kind6', Integer),
        Column('threepair', Integer),
        Column('straight', Integer),
        Column('twotriplets', Integer),
        Column('kind4bonus', Integer),
        Column('fullhousebonus', Integer),
        Column('score_cache', PickleType),
        Column('score_any_cache', PickleType),
        Column('score_any_by_num_cache', PickleType),
        Column('ev_cache', PickleType),
    ))

    mapper(mdls.User, Table(
        mdls.User.__tablename__, metadata,
        Column('id', Integer, primary_key=True),
        Column('created', DateTime),
        Column('modified', DateTime),
        Column('username', String(128), unique=True),
        Column('first', String(64)),
        Column('last', String(64)),
        Column('email', String(128), unique=True),
        Column('nickname', String(64)),
        Column('password', String(128)),
        Column('active', Boolean()),
        Column('confirmed_at', DateTime()),
    ), properties={
        'games': relationship(
            Game, secondary=Table(
                'games_users', metadata,
                Column(
                    'game_id', Integer,
                    ForeignKey(Game.__tablename__ + '.id')
                ),
                Column(
                    'user_id', Integer,
                    ForeignKey(mdls.User.__tablename__ + '.id')
                ),
            ),
            backref=backref('users')),
        'roles': relationship(
            mdls.Role, secondary=Table(
                'roles_users', metadata,
                Column(
                    'role_id', Integer,
                    ForeignKey(mdls.Role.__tablename__ + '.id')
                ),
                Column(
                    'user_id', Integer,
                    ForeignKey(mdls.User.__tablename__ + '.id')
                ),
            ),
            backref=backref('users')),
    })

    return metadata
