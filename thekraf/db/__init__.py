"""
thekraf.db
==========

Database models, ORM, and other configuration

Overview
--------

:class:`thekraf.db.model` defines models that are mostly pure Python.  Then,
:class:`thekraf.db.mapper` configures those models to be accessible via
the sqlalchemy ORM.  Separating these roles, allows for the possibility of
implementation of additional ORMs in the future.
"""
