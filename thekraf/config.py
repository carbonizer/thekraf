"""
thekraf.config
==============

Global configuration for the whole package
"""
import inspect
import json
import misaka
import os
import textwrap
import yaml

PKG_NAME = 'thekraf'

PKG_ROOT = os.path.dirname(
    os.path.abspath(inspect.getfile(inspect.currentframe())))

PKG_CONFIGS_ROOT = os.path.join(PKG_ROOT, 'db', 'configs')
PKG_CONFIGS_DEFAULT_PATH = os.path.join(PKG_CONFIGS_ROOT, 'default.yaml')


def load_config_file(path):
    """Load config data from a YAML or JSON file

    Args:
        path (str): Path of file

    Returns:
        dict: Data loaded from the file
    """
    known_exts = ('.json', '.yaml', '.yml')

    if not os.path.exists(path):
        raise FileNotFoundError(repr(path))

    _, ext = os.path.splitext(path)
    if ext.lower() not in known_exts:
        raise TypeError(
            'File {} has ext {} which is not in {}'.format(
                *map(repr, (path, ext.lower(), known_exts))
            ))

    # Load from yaml
    if ext.lower() in ('.yaml', '.yml'):
        with open(path) as fp:
            data = yaml.load(fp)

    # Load from json
    elif ext.lower() in ('.json',):
        with open(path) as fp:
            data = json.load(fp)

    return data

DEFAULTS = load_config_file(PKG_CONFIGS_DEFAULT_PATH)

PKG_VAR_ROOT = os.path.expandvars(os.path.join(
    '$HOME',
    '.' + PKG_NAME,
))

PKG_LOG_ROOT = os.path.join(PKG_VAR_ROOT, 'logs')

# *db config*

DB_PATH = os.path.join(PKG_VAR_ROOT, PKG_NAME + '.db')

# flaskapp config

WEB_HOST = '0.0.0.0'
WEB_PORT = 5000

SECRET_KEY_PATH = os.path.join(PKG_VAR_ROOT, 'secret_key')

SERVER_CRT_PATH = os.path.join(PKG_VAR_ROOT, 'server.crt')
SERVER_KEY_PATH = os.path.join(PKG_VAR_ROOT, 'server.key')

_md = misaka.Markdown(misaka.HtmlRenderer())

HTML = {k: _md(textwrap.dedent(str_))
        for k, str_ in DEFAULTS['default']['markdown'].items()}


class FlaskAppConfig(object):
    # http://flask.pocoo.org/docs/latest/config/
    LOGGER_NAME = 'web'

    # http://pythonhosted.org/Flask-Security/configuration.html
    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    SECURITY_PASSWORD_SALT = (
        'Although this is constant, user passwords are stored '
        'with a unique salt'
    )
    SECURITY_LOGIN_USER_TEMPLATE = 'login.hamlish',
    SECURITY_REGISTER_USER_TEMPLATE = 'register.hamlish',
    SECURITY_REGISTERABLE = True
    SECURITY_SEND_REGISTER_EMAIL = False
    SECURITY_USER_IDENTITY_ATTRIBUTES = ['email', 'username']

    # http://flask-sqlalchemy.pocoo.org/2.1/config/
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DB_PATH
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # SSLIFY_PERMANENT = True
    # SSLIFY_SUBDOMAINS = True


# multilogr config
LOGS_SPEC = {
    'thk': {
        'log_path': os.path.join(PKG_LOG_ROOT, PKG_NAME + '.log'),
        'levels': {
            'console': 'DEBUG',
            'file': 'DEBUG',
        },
    },
    FlaskAppConfig.LOGGER_NAME: {
        'log_path': os.path.join(PKG_LOG_ROOT,
                                 FlaskAppConfig.LOGGER_NAME + '.log'),
        'levels': {
            'console': 'DEBUG',
            'file': 'DEBUG',
        },
    },
    'werkzeug': {
        'log_path': os.path.join(PKG_LOG_ROOT,
                                 FlaskAppConfig.LOGGER_NAME + '.log'),
        'levels': {
            'console': 'DEBUG',
            'file': 'DEBUG',
        },
    },
}

# *game config*

GAME_DICE_MAX_VAL = 6
GAME_DICE_VALS = tuple(range(1, GAME_DICE_MAX_VAL + 1))
GAME_DICE_MAX_COUNT = 6

GAME_MODES = {
    'rounds': {
        'goal': 10,
        'min_first_bank': 300,
        'min_bank': 50,
    },
    'points': {
        'goal': 10000,
        'min_first_bank': 500,
        'min_bank': 50,
    }
}


GAME_OPTS = {}


# TODO: Add reference to config doc
def load_config_item(name, data=None, config_name=None, select=None):
    """Select a section of a config set

    Refer to config documentation for more info.

    Args:
        name (str): Name of the section of the config set
        data (dict): Dict of config sets
        config_name (str): Name of a config set in `data`
        select (collections.abc.Callable): A function of one argument used to
            filter items from a config section list

    Returns:
        list|tuple: Subset of data from config section

    """
    if data is None:
        data = DEFAULTS
    if config_name is None:
        config_name = 'default'
    config_data = data.get(config_name, {})
    item_data = config_data.get(name, [])

    if select:
        if isinstance(item_data, dict):
            return {k: v for k, v in item_data.items() if select((k, v))}
        else:
            return [a for a in item_data if select(a)]

    return item_data

for dirpath in (PKG_VAR_ROOT, PKG_LOG_ROOT):
    if not os.path.exists(dirpath):
        os.mkdir(dirpath, 0o755)
