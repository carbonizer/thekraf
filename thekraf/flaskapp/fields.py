"""
thekraf.flaskapp.fields
=======================

Fields used by :mod:`thekraf.flaskapp.forms`
"""
from wtforms import FieldList, Field
from wtforms.compat import text_type
from thekraf.flaskapp.widgets import CheckboxValueInput, BootstrapButtonGroupWidget


class BooleanFieldList(FieldList):
    widget = BootstrapButtonGroupWidget()


class CheckboxField(Field):
    """Checkbox input with checked state separate from value

    Represents an ``<input type="checkbox">``.  If the default value is a
    tuple, the first item will become value, and the second item is a boolean
    indicating the initial ``checked`` state.  When form data is parsed, only
    ``checked`` fields will be included (that is how HTML is designed).
    """
    widget = CheckboxValueInput()

    def __init__(self, label=None, validators=None, **kwargs):
        self.checked = False
        super(CheckboxField, self).__init__(label, validators, **kwargs)

    def process_data(self, value):
        # When creating the form, if the value is a tuple, the second item
        # will set checked
        if isinstance(value, tuple):
            value, checked = tuple(value)
            self.checked = checked
        self.data = value

    def process_formdata(self, valuelist):
        # Since the field is included in formdata, it must have been checked
        if valuelist:
            self.data = valuelist[0]
            self.checked = True

    def _value(self):
        if self.raw_data:
            return text_type(self.raw_data[0])
        else:
            return text_type(self.data)
