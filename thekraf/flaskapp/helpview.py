"""
thekraf.flaskapp.helpview
==========================

Help page for the site
"""
from thekraf.flaskapp.baseview import ViewBase
from thekraf import Multilogr

logr = Multilogr.get_logr('web')


class HelpView(ViewBase):
    pass
