"""
thekraf.flaskapp.nav
====================

Configuration of the navigation bar elements
"""
import flask
from flask_security import current_user


def _nav_elems_from_endpoints(*endpoints):
    """Create nav element dicts for the templates

    Args:
        *endpoints (str): Endpoint names

    Returns:
        list[dict]: Nav element dicts
    """
    mapped_endpoints = tuple(
        flask.current_app.url_map._rules_by_endpoint.keys()
    )
    elems = []
    for endpoint in endpoints:
        if '.' in endpoint:
            name = endpoint.rsplit('.', 1)[1]
        elif ':' in endpoint:
            name = endpoint.rsplit(':', 1)[1]
        else:
            name = endpoint
        elem = {
            'name': name,
            'endpoint': endpoint,
            'title': name.replace('_', ' ').title(),
            'href': flask.url_for(endpoint)
            if endpoint in mapped_endpoints else '#',
            'disabled': endpoint not in mapped_endpoints,
            'active': flask.request.endpoint == endpoint,
        }
        elems.append(elem)

    return elems


def top_nav_elems():
    """Configure the top navbar elements for the template

    Returns:
        tuple[dict[str, str]]: Info for each element
    """
    elems = _nav_elems_from_endpoints(
        'game',
        'newgame',
        'analysis',
        'help',
    )
    if current_user.is_anonymous:
        elems.extend(_nav_elems_from_endpoints(
            'security.login',
        ))

    else:
        elems.extend(_nav_elems_from_endpoints(
            'security.logout',
        ))

    for elem in elems:
        if elem['endpoint'] == 'newgame':
            elem['title'] = 'New Game'
        elif elem['endpoint'] == 'security.logout':
            elem['title'] += ' ({})'.format(current_user.nickname)

    return elems
