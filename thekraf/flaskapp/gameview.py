"""
thekraf.flaskapp.gameview
=========================

The main game view
"""
import flask
from flask_security import current_user

from thekraf import Multilogr, dbm
from thekraf.diceutils import DiceUtils
from thekraf.flaskapp import forms as forms
from thekraf.flaskapp.baseview import ViewBase
import thekraf.flaskapp.formatters as formatters
from thekraf.flaskapp.restapi import dict_for_frontend
from thekraf.game import Game

logr = Multilogr.get_logr('web')


class GameViewBase(ViewBase):
    @classmethod
    def load_game(cls):
        """Load game from database using info on the web session

        Returns:
            Game: The related game
        """
        sess = dbm.Session()

        game_id = flask.session.get('game_id')
        if game_id is not None:
            game = sess.query(Game).get(game_id)
        else:
            game = None

        if game and current_user.is_anonymous:
            game.player_names = flask.session['names']

        return game

    @classmethod
    def validate_session(cls):
        """Check that game info on web session is valid and matches database"""
        user = current_user
        is_anon = user.is_anonymous

        if is_anon:
            if 'game_id' in flask.session and 'names' in flask.session:
                game = cls.load_game()
                if not game or not game.is_anonymous:
                    flask.session.pop('game_id', None)
                    flask.session.pop('names', None)
            else:
                flask.session.pop('game_id', None)
                flask.session.pop('names', None)

        else:
            # If game is indicated on session
            if 'game_id' in flask.session:
                game = cls.load_game()
                # Ignore a game if it does not exist
                if not game:
                    flask.session.pop('game_id', None)
                    flask.session.pop('names', None)
                # Delete an anonymous game a user was playing before login
                elif game.is_anonymous:
                    flask.session.pop('game_id', None)
                    flask.session.pop('names', None)
                    sess = dbm.Session()
                    sess.delete(game)

            # If user has any games on file, select the last one
            if user.games:
                flask.session['game_id'] = current_user.games[-1].id

    @classmethod
    def save_game_info(cls, game):
        """Save game info to the web session

        Args:
            game (Game): Game from which to gather info

        """
        flask.session['game_id'] = game.id

        # Players of anonymous games are not in the db so that info must
        # be kept on the session
        if current_user.is_anonymous:
            flask.session['names'] = game.player_names


class GameView(GameViewBase):
    class TemplateContext(GameViewBase.TemplateContext):
        def __init__(self):
            ViewBase.TemplateContext.__init__(self)
            self.turn_form = forms.TurnForm()
            self.game_info = ()
            self.turn_info = ()
            self.game = None
            self.opts = None
            self.score_table_headers = None
            self.score_table_rows = None
            self.turn_table_headers = None
            self.turn_table_rows = None
            self.score_col_widths = None
            self.turn_col_widths = None
            self.min_scorable = ''

    @staticmethod
    def update_dice_select(ctx):
        """Update the dice selects to reflect current state of the roll

        Args:
            ctx (TemplateContext): The template context

        """
        rolled_select = ctx.turn_form.dice_select
        scored_select = ctx.turn_form.scored_select
        turn = ctx.game.turn

        # Rolled dice that haven't been scored
        remaining = DiceUtils.diff(turn.rolled, turn.scored_dice)

        while rolled_select.entries:
            rolled_select.pop_entry()

        for die in sorted(remaining):
            rolled_select.append_entry(die)

        while scored_select.entries:
            scored_select.pop_entry()

        for die in sorted(turn.scored_dice):
            scored_select.append_entry(die)

    @staticmethod
    def update_button_labels(ctx):
        """Add dynamic info to button labels

        Args:
            ctx (TemplateContext): The template context

        """
        # Default label text to title version of name
        label_texts = {k: k.replace('_', ' ').title()
                       for k in ctx.turn_form.action_buttons.keys()}

        # Indicate why the bank button would be dimmed
        if all((
                not ctx.game.turn.busted,
                not ctx.game.bankable(),
                ctx.game.turn.min_bank > 50
        )):
            label_texts['bank'] += ' ({} min)'.format(ctx.game.turn.min_bank)

        # Indicate that rather than continuing to roll dice for this turn,
        # play is being passed to the next turn/player
        if not ctx.game.complete and \
                (ctx.game.turn.complete or ctx.game.turn.busted):
            label_texts['roll'] = 'Next'

        # Indicate which dice will be selected by clicking score min
        if ctx.game.turn.min_scorable:
            ctx.min_scorable = ctx.game.turn.min_scorable[0]

        # Update labels on corresponding button
        for name, btn in ctx.turn_form.action_buttons.items():
            btn.label.text = label_texts[name]

    @staticmethod
    def update_button_states(ctx):
        """Update enabled/disabled state of the button inputs

        Args:
            ctx (TemplateContext): The template context

        """
        # Default everything disabled
        d = {k: True for k in ctx.turn_form.action_buttons.keys()}

        # Enable buttons as needed
        if not ctx.game.complete and any((ctx.game.is_anonymous,
                                          current_user == ctx.game.player)):
            if ctx.game.bankable():
                d['bank'] = False
            if ctx.game.turn.score or ctx.game.turn.busted:
                d['roll'] = False
            if ctx.turn_form.dice_select.entries and not ctx.game.turn.busted:
                d['score'] = False
            if ctx.game.turn.min_scorable:
                d['score_min'] = False
            if ctx.turn_form.scored_select.entries:
                d['unscore'] = False

        # Apply disabled state to the button corresponding to each action
        for name, btn in ctx.turn_form.action_buttons.items():
            render_kw = getattr(btn, 'render_kw')
            if render_kw is None:
                render_kw = {}
            render_kw['disabled'] = d[name]
            btn.render_kw = render_kw

    def get(self):
        ctx = self.ctx

        self.validate_session()

        if 'game_id' not in flask.session:
            return flask.redirect(flask.url_for('newgame'))

        ctx.game = self.load_game()

        if ctx.game.complete:
            flask.flash('Game finished', 'info')

        elif ctx.game.turn.busted:
            if current_user.is_anonymous or \
                            ctx.game.turn.pid == current_user.id:
                flask.flash(
                    'Prepare to curse wildly; your last roll was a bust.',
                    'warning'
                )

        return self.render()

    def post(self):
        ctx = self.ctx

        # Turn form submitted
        if ctx.turn_form.validate_on_submit():
            # Reconstitute the game instance
            ctx.game = self.load_game()

            # Buttons use a naming convention of <action>_button
            # so the button clicked indicates the action
            action = [name[0:len(name) - len('_button')]
                      for name, val in ctx.turn_form.data.items()
                      if name.endswith('_button') and val][0]

            # Create params dict
            d = {
                'dice': ctx.turn_form.dice_select.data,
            }
            if action == 'unscore':
                d['dice'] = ctx.turn_form.scored_select.data

            # Perform the action in the game
            ctx.game.do_action(action, d)
            dbm.Session().commit()
            self.save_game_info(ctx.game)

        logr.debug('turn_form: ' + repr(ctx.turn_form.data))

        return self.redirect_to_get()

    def before_render(self):
        ctx = self.ctx

        self.update_dice_select(ctx)

        self.update_button_states(ctx)
        self.update_button_labels(ctx)

        # Update game and turn info for template
        ctx.game_info = ctx.game.game_info()
        ctx.turn_info = ctx.game.turn_info()
        ctx.score_table_headers, ctx.score_table_rows = \
            ctx.game.score_table_info()
        ctx.turn_table_headers, ctx.turn_table_rows = \
            ctx.game.turn_table_info()
        ctx.turn_col_widths = {
            'Roll': '1',
            'Score': '1',
            'Dice': '3',
        }
        assert set(ctx.turn_col_widths) == set(ctx.turn_table_headers)
        ctx.score_col_widths = {
            'Player': '2',
            'Total': '2',
        }
        assert set(ctx.score_col_widths).issubset(set(ctx.score_table_headers))
        ctx.opts = formatters.score_opts_def_pairs(ctx.game.opts)

        # Create data to be available to the frontend
        ctx.update_frontend({
            'game': dict_for_frontend(ctx.game),
            'user': None if current_user.is_anonymous
            else dict_for_frontend(current_user),
        })
