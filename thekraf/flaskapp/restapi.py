"""
thekraf.flaskapp.restapi

Definition of the REST API for db access by frontend
"""
import flask
from flask_classy import FlaskView
from werkzeug.local import LocalProxy
from thekraf import dbm
import thekraf.db.models as mdls
from thekraf.game import Game
from thekraf.gamemodels import Player


def dict_for_frontend(obj):
    """Create jsonifiable dict of select object attrs

    Args:
        obj (object): Target object

    Returns:
        dict: Jsonifiable dict
    """
    d = {}
    # Unwrap proxy if necessary
    if isinstance(obj, LocalProxy):
        obj = obj._get_current_object()

    if isinstance(obj, Game):
        d = {k: getattr(obj, k) for k in ('id', 'modified', 'player')}
        d['player'] = dict_for_frontend(d['player'])
    elif isinstance(obj, (mdls.User, Player)):
        d = {k: getattr(obj, k) for k in ('id', 'nickname')}
    return d


class GameView(FlaskView):
    route_prefix = '/api/'

    def get(self, ident):
        sess = dbm.Session()
        game = sess.query(Game).get(ident)
        d = dict_for_frontend(game)
        return flask.jsonify(**d)
