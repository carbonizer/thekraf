"""
thekraf.flaskapp.formatters
===========================

Functions to format model data for the backend templates
"""
import thekraf.db.models as mdls


def score_opts_def_pairs(opts):
    """Attr name / value pairs of specific opts

    Args:
        opts (ScoreOptions): Scoring options

    Returns:
        tuple[tuple[str, str]]: Attr name / value pairs
    """
    pairs = tuple((name, getattr(opts, name, '???')) for name in
                  'name description fourplusscheme single1 single5 triple1 '
                  'straight threepair twotriplets kind4bonus '
                  'points'.split())
    return pairs
