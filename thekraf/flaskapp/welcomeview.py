"""
thekraf.flaskapp.welcomeview
==========================

Landing page for the site
"""
from flask_security import current_user

from thekraf.flaskapp.baseview import ViewBase
from thekraf import Multilogr

logr = Multilogr.get_logr('web')


class WelcomeView(ViewBase):
    class TemplateContext(ViewBase.TemplateContext):
        def __init__(self):
            ViewBase.TemplateContext.__init__(self)
            self.user = None if current_user.is_anonymous else current_user
