"""
thekraf.flaskapp.widgets
========================

Widgets used by :mod:`thekraf.flaskapp.fields`
"""
from wtforms.widgets import HTMLString, html_params, Input


class BootstrapButtonGroupWidget(object):
    """Renders a Bootstrap 3 checkbox/radio button group"""

    def __init__(self):
        pass

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)

        # Parse button group styles
        btn_group_classes = ['btn-group']
        for style_suffix in kwargs.pop('_btn_group_styles', []):
            btn_group_classes.append('btn-group-{}'.format(style_suffix))
        html = ['<div class="{}" data-toggle="buttons" {}>'.format(
            ' '.join(btn_group_classes),
            html_params(**kwargs),
        )]

        # Parse the button styles for the label
        btn_classes = ['btn']
        for style_suffix in kwargs.pop('_btn_styles', []):
            btn_classes.append('btn-{}'.format(style_suffix))

        for subfield in field:
            label_classes = btn_classes[:]
            if getattr(subfield, 'checked', False):
                label_classes.append('active')
            html.append('<label class="{}">'.format(' '.join(label_classes)))
            html.append(subfield())
            html.append(subfield._value())
            # html.append('<input type="hidden" name="{}" value="false" />'.format(subfield.name))
            html.append('</label>')
        html.append('</div>')
        return HTMLString(''.join(html))


class CheckboxValueInput(Input):
    """Render a checkbox with checked state separate from value

    The ``checked`` HTML attribute is set if `field.checked` is a non-false
    value.  The value of the input doesn't change.  This allows using the value
    in labeling the field.

    .. note ::
        HTML will only return checkbox inputs that are checked upon submit.
    """
    input_type = 'checkbox'

    def __call__(self, field, **kwargs):
        if getattr(field, 'checked', False):
            kwargs['checked'] = True
        return super(CheckboxValueInput, self).__call__(field, **kwargs)
