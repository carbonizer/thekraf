"""
thekraf.flaskapp.newgameview

View for creating a new game
"""
import flask
from flask_security import current_user

import thekraf.config as cfg
from thekraf.flaskapp import forms as forms
from thekraf import Multilogr, dbm
import thekraf.db.models as mdls
from thekraf.flaskapp.gameview import GameViewBase
from thekraf.game import Game

logr = Multilogr.get_logr('web')


class NewGameView(GameViewBase):
    class TemplateContext(GameViewBase.TemplateContext):
        def __init__(self):
            GameViewBase.TemplateContext.__init__(self)
            self.game_form = forms.GameForm()
            self.game_info = ()
            self.game = None

            # Configure the select choices
            self.game_form.opts_select.choices = [
                [k, k + (' -- {}'.format(v.description)
                         if v.description else '')]
                for k, v in sorted(cfg.GAME_OPTS.items())
            ]

    @classmethod
    def new_game(cls, game_form=None):
        """Create a new game / reset game

        Args:
            game_form (forms.GameForm): The form that if posted, would execute
                this method

        """
        # Configure default game parameters
        d = {
            'mode': 'rounds',
        }
        if current_user.is_anonymous:
            d['is_anonymous'] = True
            d['users'] = ('Player 1',)
        else:
            d['users'] = (current_user,)

        sess = dbm.Session()

        # Use data from the new game form
        if game_form:
            d['mode'] = game_form.mode_select.data
            # Player names for an anonymous game are provided by a comma
            # delimited list
            if current_user.is_anonymous:
                if game_form.player_name_input.data:
                    d['users'] = [
                        name.strip()
                        for name in game_form.player_name_input.data.split(',')
                        ]
            # Create game from selected users
            elif game_form.user_select.data:
                users = sess.query(mdls.User).filter(
                    mdls.User.username.in_(game_form.user_select.data)
                ).all()
                d['users'] = users

        d['opts'] = cfg.GAME_OPTS.get(game_form.opts_select.data,
                                      cfg.GAME_OPTS['default'])

        game = Game(**d)

        # Save to db
        sess.add(game)
        sess.commit()

        cls.save_game_info(game)

        flask.flash('Created a new game', 'info')

    @staticmethod
    def update_player_select(ctx):
        """Update the player selects to include users

        Args:
            ctx (TemplateContext): The template context

        """
        user_select = ctx.game_form.user_select

        while user_select.entries:
            user_select.pop_entry()

        sess = dbm.Session()
        for username, in sess.query(mdls.User.username).all():
            if username == current_user.username:
                user_select.append_entry((username, True))
            else:
                user_select.append_entry(username)

    def get(self):
        ctx = self.ctx

        self.validate_session()

        # Warning about incomplete existing game
        if 'game_id' in flask.session:
            ctx.game = self.load_game()
            if not ctx.game.complete:
                flask.flash('Your latest game is incomplete, and you may only '
                            'play one game at a time', 'warning')

        return self.render()

    def post(self):
        ctx = self.ctx

        logr.debug('game_form: ' + repr(ctx.game_form.data))

        # New game form submitted
        if ctx.game_form.validate_on_submit():
            self.new_game(ctx.game_form)
            ctx.game = self.load_game()

            # Redirect to get game
            return self.redirect_to_get('game')
        else:
            return self.render()

    def before_render(self):
        # Preselect default scoring options
        self.ctx.game_form.opts_select.data = 'default'
        # There are two methods to choose players.
        # Choose between the two, and pop the other off the form.
        # Anonymous users use a manual name input
        if current_user.is_anonymous:
            self.ctx.game_form._fields.pop('user_select')
        # Otherwise, choose from a list of users
        else:
            self.update_player_select(self.ctx)
            self.ctx.game_form._fields.pop('player_name_input')
