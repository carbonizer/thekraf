"""
thekraf.flaskapp.forms
======================

Flask-WTF forms used by the flask app
"""
from flask_security.forms import LoginForm, RegisterForm
from flask_wtf import Form as BaseForm
from wtforms import (BooleanField, StringField, SubmitField, ValidationError,
                     SelectField, PasswordField)
from wtforms.validators import DataRequired, Optional

import thekraf.config as cfg
import thekraf.diceutils as diceutils
from thekraf.flaskapp.fields import BooleanFieldList, CheckboxField
from thekraf.game import Game
from thekraf.utils import recreate_field


class Form(BaseForm):

    ACTION_SUFFIX = '_button'

    @property
    def action_buttons(self):
        len_suffix = len(self.ACTION_SUFFIX)
        return {name[0:-len_suffix]: fld for name, fld in self._fields.items()
                if isinstance(fld, SubmitField) and
                name.endswith(self.ACTION_SUFFIX)}


class GameForm(Form):
    mode_select = SelectField('Mode',
                              choices=[(k, k.title())for k in Game.MODES])
    opts_select = SelectField('Scoring Config',
                              choices=[(k, k) for k in cfg.GAME_OPTS])
    player_name_input = StringField(
        'Player Names', render_kw=dict(placeholder='Player 1, Player 2, ...')
    )
    user_select = BooleanFieldList(
        CheckboxField(render_kw={'autocomplete': 'off'}),
        label='Players',
        render_kw={'_btn_group_styles': ('vertical',),
                   '_btn_styles': ('default',)}
    )
    reset_button = SubmitField('Create',
                               render_kw=dict(class_='btn btn-default'))


class LoginUserForm(LoginForm):
    email = StringField('E-mail/Username', validators=(DataRequired(),))
    password = PasswordField('Password', validators=(DataRequired(),))
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class RegisterUserForm(RegisterForm):
    email = recreate_field(RegisterForm.email)
    username = StringField('Username', validators=(Optional(),))
    password = recreate_field(RegisterForm.password)
    password_confirm = recreate_field(RegisterForm.password_confirm)
    first = StringField('First Name *', validators=(DataRequired(),))
    last = StringField('Last Name *', validators=(DataRequired(),))
    nickname = StringField('Nickname', validators=(Optional(),))
    submit = recreate_field(RegisterForm.submit)

    def to_dict(self):
        return {k: self._fields[k].data for k in ('email', 'username',
                                                  'password', 'first',
                                                  'last', 'nickname')
                if self._fields[k].data}


class TurnForm(Form):
    dice_select = BooleanFieldList(
        CheckboxField(render_kw={'autocomplete': 'off'}),
        label='Select Dice',
        render_kw={'_btn_group_styles': ('lg',), '_btn_styles': ('primary btn-dice',)}
    )

    score_button = SubmitField('Score')
    score_min_button = SubmitField('Score Min')
    roll_button = SubmitField('Roll', render_kw=dict(disabled=True))

    scored_select = BooleanFieldList(
        CheckboxField(render_kw={'autocomplete': 'off'}),
        label='Scored Dice',
        render_kw={'_btn_group_styles': ('lg',), '_btn_styles': ('success',)}
    )

    unscore_button = SubmitField('Unscore', render_kw=dict(disabled=True))
    bank_button = SubmitField('Bank', render_kw=dict(disabled=True))


class ScoreForm(Form):
    dice_input = StringField('Dice Repr')
    submit = SubmitField('Score')

    def validate_dice_input(self, field):
        try:
            diceutils.DiceUtils.create_dice(field.data)
        except (TypeError, ValueError) as e:
            raise ValidationError(repr(e))


class OptsForm(Form):
    opts_select = SelectField('Scoring Config',
                              choices=[(k, k) for k in cfg.GAME_OPTS])
    submit = SubmitField('Load')
