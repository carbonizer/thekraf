"""
thekraf.flaskapp.app
====================

Create and configure the global app instance

Production servers usually need to access the global app instance.
"""
from thekraf import load_game_opts

from thekraf.flaskapp import create_app
from thekraf.flaskapp.routes import config_hello_route, config_routes


def create_and_config_hello_app():
    """Create and configure a simple app, helpful for some unit testing

    Returns:
        flask.Flask: Simple hello app
    """
    return config_hello_route(create_app())


def create_and_config_app():
    """Create and configure the main app

        Returns:
            flask.Flask: Main app instance
    """
    app_ = config_routes(create_app())

    # Opts are loaded in thekraf.__main__.main(), but this function isn't run if
    # a production server is running the app.  This will ensure that they are
    # load in that situation.
    app_.before_first_request(load_game_opts)

    return app_


app = create_and_config_app()
"""flask.Flask: Global flask app instance"""
