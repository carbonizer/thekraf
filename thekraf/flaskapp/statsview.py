"""
thekraf.flaskapp.statsview
==========================

A view for probabilistic analysis
"""
import string

import thekraf.config as cfg
from thekraf.flaskapp.baseview import ViewBase
import thekraf.flaskapp.formatters as formatters
import thekraf.flaskapp.forms as forms
from thekraf import Multilogr
import thekraf.stats as stats

logr = Multilogr.get_logr('web')


class StatsView(ViewBase):
    class TemplateContext(ViewBase.TemplateContext):
        def __init__(self):
            ViewBase.TemplateContext.__init__(self)
            self.form = forms.OptsForm()
            self.headers = None
            self.rows = None
            self.opts = None

            # Configure the select choices
            self.form.opts_select.choices = [
                [k, k + (' -- {}'.format(v.description)
                         if v.description else '')]
                for k, v in sorted(cfg.GAME_OPTS.items())
            ]

    @classmethod
    def format_ev_cache(cls, ctx, cache, opts=None):
        """Format ev cache for display as a table"""
        data = stats.formatted_ev_cache(opts)

        def _figure_cell_class(val):
            if '.' in val:
                return 'success'
            elif all(c in string.digits for c in val):
                return 'danger'
            else:
                return None

        ctx.headers = data[0]
        ctx.rows = [(r[0], [(c, _figure_cell_class(c))
                            for c in r[1:]]) for r in data[1:]]

    @classmethod
    def format_opts(cls, ctx, opts):
        """Format specific opts for display as a definition list

        Args:
            ctx (TemplateContext): Template context
            opts (ScoreOptions): Scoring options
        """
        pairs = formatters.score_opts_def_pairs(opts)
        ctx.opts = pairs

    def get(self):
        return self.render()

    def post(self):
        ctx = self.ctx

        if ctx.form.validate_on_submit():
            # Use the selection as a key later
            ctx.opts = ctx.form.opts_select.data

        return self.render()

    def before_render(self):
        ctx = self.ctx
        # If ctx.opts is a key, get related opts, else use default
        opts = cfg.GAME_OPTS.get(ctx.opts, cfg.GAME_OPTS['default'])
        # Pre-select the loaded config
        ctx.form.opts_select.data = opts.name
        self.format_opts(ctx, opts)
        self.format_ev_cache(ctx, opts.ev_cache, opts=opts)
