"""
thekraf.flaskapp.__main__
=========================

Entry-point for running running the web app web server

:mod:`thekraf.__main__` imports from this module to create the "web" subcommand
"""
import argparse
import os
import sys

import parawrap

from thekraf import config as cfg
from thekraf.flaskapp.app import app
from thekraf.utils import MixedHelpFormatter


def parse_args(args):
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    config_web_parser(subparsers.add_parser('web'))
    return parser.parse_args(args)


def config_web_parser(parser):
    parser.description = parawrap.fill(
        'Run builtin dev server or print a command that would run the server '
        'if the command were evaluated.  Although the production server will '
        'not be run directly, it could be run with:\n'
        '\n'
        '\t\'eval $(thekraf web [OPTIONS] <production-server>)\''
    )
    parser.formatter_class = MixedHelpFormatter
    parser.add_argument('--debug', '-d',
                        action='store_true',
                        help='Run the dev server with debug web iface and '
                             'reload server on source file changes')
    parser.add_argument('--host',
                        default=cfg.WEB_HOST,
                        help='Server host/name')
    parser.add_argument('--port', '-p',
                        type=int, default=cfg.WEB_PORT,
                        help='Port on which the server listens')
    parser.add_argument('--ssl',
                        action='store_true',
                        help='Run server with SSL (requires configuring '
                             'server.crt and server.key)')
    parser.add_argument('server',
                        choices=('builtin', 'gunicorn'), default='builtin',
                        help='Run builtin dev server or print command '
                             'related to running a specific production server')
    parser.set_defaults(func=web_cmd)


def web_cmd(pargs):
    """Run web server"""
    d = {
        'host': pargs.host,
        'port': pargs.port,
        'debug': pargs.debug,
        'extra_files': [cfg.PKG_CONFIGS_DEFAULT_PATH],
    }

    # If ssl cert and key are in place, add an ssl context
    if all((pargs.ssl, all(map(os.path.exists, (cfg.SERVER_CRT_PATH,
                                                cfg.SERVER_KEY_PATH))))):
        d['ssl_context'] = (cfg.SERVER_CRT_PATH, cfg.SERVER_KEY_PATH)

    if pargs.server == 'builtin':
        app.run(**d)
    else:
        print('Unable to run server internally, you can run:', file=sys.stderr)
        cmd = None
        if pargs.server == 'gunicorn':
            chunks = ['gunicorn -b{host}:{port}']
            if 'ssl_context' in d:
                chunks.append(
                    '--certfile {ssl_context[0]} --keyfile {ssl_context[1]}'
                )
            chunks.append('thekraf.flaskapp.app:app')
            cmd = ' '.join(chunks).format(**d)
        return cmd


def main(args=sys.argv[1:]):
    pargs = parse_args(args)
    web_cmd(pargs)


if __name__ == '__main__':
    main()
