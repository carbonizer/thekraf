"""
thekraf.flaskapp.scoreview
==========================

A view for a form to score dice from a string representation
"""
from collections import namedtuple

import flask
from flask.views import MethodView

import thekraf.flaskapp.forms as forms
from thekraf import Multilogr
from thekraf.diceutils import DiceUtils
from thekraf.score import Score

logr = Multilogr.get_logr('web')


class MyData(object):
    diceout = [(1, False), (3, True), (5, False)]

ScoreData = namedtuple('ScoreData', 'dice_select')


class ScoreView(MethodView):
    class TemplateContext(object):
        form = None
        dice = []
        score = ''

    def get(self):
        ctx = self.TemplateContext()
        ctx.form = forms.ScoreForm()
        return self.render(ctx)

    def post(self):
        ctx = self.TemplateContext()
        ctx.form = forms.ScoreForm()
        if ctx.form.validate_on_submit():
            ctx.dice = DiceUtils.create_dice(ctx.form.dice_input.data)
            ctx.score = Score.calc_score(ctx.dice)
        return self.render(ctx)

    @staticmethod
    def render(ctx):
        return flask.render_template('score.hamlish', ctx=ctx)
