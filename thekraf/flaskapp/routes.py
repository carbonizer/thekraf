"""
thekraf.flaskapp.routes
=======================

Configures the routes of the application

Routes are usually configured on the global app instance directly.  However,
doing so in functions aids unit testing a debugging.
"""
import flask

from thekraf.flaskapp import restapi as restapi
from thekraf.flaskapp.gameview import GameView
from thekraf.flaskapp.helpview import HelpView
from thekraf.flaskapp.newgameview import NewGameView
from thekraf.flaskapp.scoreview import ScoreView
from thekraf.flaskapp.statsview import StatsView
from thekraf.flaskapp.welcomeview import WelcomeView


def config_hello_route(app):
    """Configure the single route, hello

    Args:
        app (flask.Flask): App to configure

    Returns:
        flask.Flask: Same object that was passed in
    """
    @app.route('/hello')
    def hello():
        """Good to keep around for sanity checks"""
        return 'Hello World!'

    return app


def config_routes(app):
    """Configure all app routes

    Args:
        app (flask.Flask): App to configure

    Returns:
        flask.Flask: Same object that was passed in
    """
    config_hello_route(app)

    app.add_url_rule('/game', view_func=GameView.as_view('game'))
    app.add_url_rule('/newgame', view_func=NewGameView.as_view('newgame'))
    app.add_url_rule('/help', view_func=HelpView.as_view('help'))
    app.add_url_rule('/score', view_func=ScoreView.as_view('score'))
    app.add_url_rule('/analysis', view_func=StatsView.as_view('analysis'))
    app.add_url_rule('/welcome', view_func=WelcomeView.as_view('welcome'))

    restapi.GameView.register(app)

    @app.route('/resetgame')
    def resetgame():
        NewGameView.new_game()
        return flask.redirect(flask.url_for('game'))

    @app.route('/')
    def my_index():
        return flask.redirect(flask.url_for('welcome'))

    @app.route('/betterhello')
    def betterhello():
        return flask.render_template('hello.hamlish')

    @app.route('/test')
    def whatever_test():
        return flask.render_template('test.hamlish')

    return app
