# app.coffee
# This is the entry point for the frontend code

# Include bootstrap in the bundle for the frontend
require("bootstrap-webpack");
require("../style.sass");


document.write("<!-- frontend bundle received -->");

# Update game info via the REST api
# Set up a timer to repeat the process if necessary
update_game = () ->
  $.ajax(
    dataType: 'json',
    url: 'api/game/' + ctx.game.id,
    data: {},
    success: (data, textStatus, jqXHR) ->
      last_modified = ctx.game.modified
      ctx.game = data
      # TODO: Make reload type configurable
      # If it became the user's turn, reload the page
#      if ctx.user.id == ctx.game.player.id
      # If game has changed, reload
      if ctx.game.modified != last_modified
        location.reload(true)
      # Otherwise, check again later
      else
        setTimeout(update_game, 2000)
  );

# Configure tooltips
config_tooltips = (tooltips) ->
  for tt in tooltips
    $tag = $("#{tt.selector}")
    # Wrap disabled tags, tags that already have tooltips, or manually requested
    if tt.wrap or $tag[0].disabled or $tag.attr('data-toggle') == 'tooltip'
      # Wrap tag
      $tag.wrap('<div class="tooltip-wrapper"></div>')
      # Target wrapper
      $wrapper = $tag.parent()
      # Add tooltip to wrapper
      $wrapper.tooltip(tt.opts)
    else
      # Add tooltip to tag
      $tag.tooltip(tt.opts)

teardown_tooltips = (tooltips) ->
  for tt in tooltips
    $tag = $("#{tt.selector}")
    # If a tooltip wrapper was used
    if tt.wrap or $tag[0].disabled or $tag.attr('data-toggle') == 'tooltip'
      $wrapper = $tag.parent()
      # Destroy tooltip on wrapper
      $wrapper.tooltip('destroy')
      # Unwrap tag
      $tag.unwrap()
    else
      # Destroy tooltip on tag
      $tag.tooltip('destroy')


# Wait for the document to be fully loaded before we execute any frontend code
$(document).ready( ->
  switch ctx.endpoint
    when 'game' then handle_game_view()
    else
      console.log("Nothing to do for endpoint #{ctx.endpoint}")
);

handle_game_view = ->
  # Config static tooltips
  config_tooltips(ctx.tooltips.onload)
  
  # Set up quick help tooltips to be enabled by toggle button
  $qht = $('#quick_help_toggle')
  $qht.click(() ->
    # Was active BEFORE click
    if $qht.hasClass('active')
      teardown_tooltips(ctx.tooltips.quick_help)
    # Was NOT active BEFORE click
    else
      config_tooltips(ctx.tooltips.quick_help)
  )

  # If not the user's turn, check the server periodically for when
  # it becomes the user's turn
  if ctx.user
#    if ctx.user.id != ctx.game.player.id
    if ctx.user
      setTimeout(update_game, 2000)
    else
      console.log('No auto-reloading because it is the user\'s turn')
  else
    console.log('No auto-reloading for an anonymous user')
