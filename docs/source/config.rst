Configuration
=============

.. include:: _inc/refs.rst

Configuration Files
-------------------

Configuration files are written in YAML.  The topmost collection is a dict of
configuration sets.  Generally, a config file will have default config set
(named "default") that is used by default.  Additional config sets may be
defined and used instead of the default.

.. todo::
    Enable cascading config sets.

The structure of a config file is as follows::

    # Default config set name is "default"
    default:
      # A config set is a dict from section names to section config data
      section_name_0:
        # One common structure for section data is a list of dicts. Each dict
        # contains an item id and one or more values.  The result is an ordered
        # dict.
        - id_key: item_0_id
          val_0_key: item_0_val_0
          val_1_key: item_0_val_1
          ...
          val_n_key: item_0_val_n
        - id_key: item_1_id
          ...
        ...
        - id_key: item_n_id
      section_name_1:
        # Another common structure for section data is a simple dict.
        key_0: val_0
        key_1: val_1
        ...
        key_n: val_n
      ...
      section_name_n:
        # Alternative structures are allowed for consumers that require
        # something special.
        "Other data structure"

    # Other config set that may be selected instead of "default"
    other_config_set_name:
      section_name_0:
        ...
      section_name_1:
        ...
      ...
      section_name_n:
        ...
