Guided Tour of the Code
=======================
.. _package_distribution: http://python-packaging-user-guide.readthedocs.io/
                                 en/latest/distributing/

Typically, this is where the API documentation would go, but this isn't a
library.  However, this is a nice overview for someone wanting to get into the
code.

Basic Structure
---------------

The project is primarily structured as a python package.  The following items in
the root of the project are common to many python package distributions (For
more info, check out `this <package_distribution_>`_:

docs/:
    Documentation for the project

tests/:
    Unit tests for the project

thekraf/:
    The main python package of the project that is installed by setuptools

MANIFEST.in:
    *setup.py* is configured to search for python code to include when
    installing the package into *site-packages*.  To include non-python files,
    one option is to reference those files with rules defined in this file.

README.rst:
    Introductory information about the project

requirements.txt:
    Python package dependencies for the project.  Although dependencies can be
    defined in *setup.py*, this file is more capable, especially when not all
    requirements are hosted on `PyPI <https://pypi.python.org/pypi>`_ (such as
    with this project).

setup.cfg:
    Site-specific setup config
    (`More info here <https://docs.python.org/3.5/distutils/configfile.html>`_).
    In this case, it is being used to configure bumpversion.

setup.py:
    Primary configuration file for package distribution.

Core Logic
----------

.. automodule:: thekraf.diceutils
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.game
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.gamemodels
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.score
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.utils
    :members:
    :undoc-members:
    :show-inheritance:

Database
--------

.. automodule:: thekraf.db
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.db.listeners
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.db.mapper
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.db.models
    :members:
    :undoc-members:
    :show-inheritance:

Web
---

Init
````

.. automodule:: thekraf.flaskapp
    :members:
    :undoc-members:
    :show-inheritance:

Views
`````

.. automodule:: thekraf.flaskapp.baseview
    :members:
    :undoc-members:
    :show-inheritance:

View Helpers
````````````

.. automodule:: thekraf.flaskapp.fields
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.flaskapp.formatters
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.flaskapp.forms
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.flaskapp.nav
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: thekraf.flaskapp.widgets
    :members:
    :undoc-members:
    :show-inheritance:

App
```

.. automodule:: thekraf.flaskapp.app
    :members:
    :undoc-members:
    :show-inheritance:


Paths
`````

static/:
    Static resources available to the client

static/frontend/:
    Frontend code

templates/:
    Backend templates

package.json:
    This defines the nodejs dependencies to install when ``node install`` is run

package.yaml:
    YAML version of package.json.  Advantageous since it allows comments.
    However, it must be converted to package.json before it can be used.

webpack.config.js:
    Configuration file that is interrogated when ``webpack`` is run.  The
    result is static/bundle.js.
