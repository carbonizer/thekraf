Running the Server
==================

.. include:: _inc/refs.rst

Development Server
------------------

Run the development web server with::

    thekraf web


Then, you can browse to `<http://0.0.0.0:5000>`_, or to prevent connections
external to localhost, use::

    thekraf web --host 127.0.0.1

Then, you can browse to `<http://127.0.0.1:5000>`_.

The default host is ``0.0.0.0``, but you may override it with the ``--host``
option. The default port is 5000, but you may override it with the ``-p``
option.  The ``-d`` flag runs the server in a debug mode which reloads the
server when code is changed and provides an interactive debugger through the
web clients interface when an error occurs.

.. note::

    ``0.0.0.0`` tells flask to allow requests external to ``localhost``.  This
    a potential security risk, so you may want to use ``127.0.0.1`` instead.

Production Server
-----------------

Ultimately, it is best to use a production level server, for example::

    gunicorn -b 0.0.0.0:5000 thekraf.flaskapp.app:app

Running a flask app using other web servers is detailed
`here <deploying-flask_>`_.
