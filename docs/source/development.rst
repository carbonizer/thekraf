Development
===========

.. include:: _inc/refs.rst

If you want to mess around with the code, there are two levels of development
to consider.  In both cases, follow the "typical" instructions first.

Typical Python/Backend Development
----------------------------------

Clone the repo and install in development mode::

    git clone https://bitbucket.org/carbonizer/thekraf &&
    pip3 install -r thekraf/requirements.txt gunicorn &&
    pip3 install -e "./thekraf[doc,dev,test]"

With the ``-e`` flag, instead of installing the package into ``site-packages``,
links are created in ``site-packages`` to some of the contents of ``thekraf``
which simulates the package be installed while reflecting changes made to
the code.  ``[doc,dev,test]`` indicates the requirements as defined by
``extras_require`` in ``setup.py`` should be installed so a developer can build
the ``doc``\ umentation, run unit ``test``\ s, and run other ``dev`` tasks.

Advanced (Changing the bundled frontend code)
---------------------------------------------

All of the frontend code is bundled (mostly in
``thekraf/thekraf/flaskapp/static/bundle.js``). This speeds up responses and
simplifies the includes in the html head.  The bundled code is already included
in the repo, so you only need to follow these steps if you wish to muck with
the frontend code.

Install nodejs/npm (More about that
`here <https://github.com/nodesource/distributions>`_)

Install ``webpack`` globally::

    npm -g install webpack

.. note::
    For global node modules (such as ``webpack``) to run, ``NODE_PATH`` must be
    defined in the environment.  You might consider running the following
    command or adding it to your shell profile::

        export NODE_PATH="$(npm root -g)"

Install the third-party libraries that will be included in the bundle::

    pushd thekraf/thekraf/flaskapp &&
    npm install &&
    popd

Each time the custom, frontend code changes (currently, that is all contained
within ``app.coffee``) the bundle must be rebuilt with::

    pushd thekraf/thekraf/flaskapp &&
    webpack
    popd

The build takes ~5 seconds.  If you are actively developing with frequent
changes to the frontend code, you may wish to leave ``webpack`` running in a
watch mode that will recompile change in milliseconds.  To do this, from
``thekraf/thekraf/flaskapp``, run::

    webpack --watch

Build Docs
----------

The docs are hosted on
`Read the Docs`_, but you can build
them yourself if from ``thekraf/docs``, you run::

    make html

After a successful build, the path of the directory containing ``index.html``
will be printed.

Run Unit Tests
--------------

From the ``thekraf`` repo root, run::

    nosetests --with-doctest

