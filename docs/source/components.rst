Components
==========

.. _Bootstrap 3: http://getbootstrap.com
.. _CoffeeScript: http://coffeescript.org
.. _Farkle: https://en.wikipedia.org/wiki/Farkle
.. _Flask: http://flask.pocoo.org
.. _Hamlish-jinja: https://github.com/Pitmairen/hamlish-jinja
.. _Python 3: https://www.python.org
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Sphinx: http://www.sphinx-doc.org/en/stable/index.html
.. _SQLAlchemy: http://www.sqlalchemy.org
.. _sqlite: https://www.sqlite.org
.. _webpack: https://webpack.github.io

Core Language
    `Python 3`_

Database
    sqlite_

Database ORM
    SQLAlchemy_

Backend Web Framework
    Flask_

Backend Web Templates
    Hamlish-jinja_

Frontend Web Framework
    None.  Currently, the logic on the frontend is minimal, and a framework would
    likely be overkill.  The logic that is there is written in CoffeeScript_.

Web Layout/Design Framework
    `Bootstrap 3`_

Web Module Bundler
    webpack_

Documentation
    Sphinx_ / reStructuredText_
