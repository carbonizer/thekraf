Getting Started
===============

.. include:: _inc/refs.rst

.. note::

    I often add details that are unnecessary for most but may be helpfully for
    those new to some of these technologies.  Sometimes installation
    instructions mention several things in passing that I must research
    elsewhere before I understand enough to complete the installation.  So,
    hopefully I can reduce the number of "I wish I had known that" moments.

Intro
-----

One game night with friends, I was introduced to the dice tossing game Farkle.
Although it was obviously a game of chance, after a few rounds, it was clear
that there was only a tiny possibility of applying skill to the game.  I wanted
to learn what I could about that small nugget of skill.  I found a
`paper <ev-paper_>`_ that included the expected values for many situations in
Farkle.  That satisfied my initial curiosity, but that static pdf didn't
provide the ability to adjust point values or play out what-if scenarios using
customizable house rules.  I was interested in creating that.

I decided to implement the game itself first because the analysis requires a
certain amount of the game mechanics anyway and acts as a good reference for
the analysis.

Python Version
--------------

This is a "Python 3 first" project.  Primary development and testing is being
done using Python 3.5.  After things are working for you using 3, you have my
permission to mess around with the code in 2.7.

When I originally learned Python way back when, I learned Python 3.  Once I
started using a handful of third-party packages, it became clear that support
for Python 3 was severely lacking.  So, I proceeded with 2.7 (while frequently
importing from the ``__future__`` module).

Years later, Python 3 adoption has improved significantly.  As such, I prefer
using Python 3 for new projects, especially personal ones.  However, since
Python 2.7 is still so heavily used and I have a history of writing for Python
2.7, I tend to write backwards compatible code.  Ultimately, I plan to have
the code fully backward compatible.

Command Line Interface
----------------------

After installation, help for the cli is available via::

    thekraf --help

Web Interface
-------------

Once the server is running, the website has a help view that provides help
specific to the web interface.
