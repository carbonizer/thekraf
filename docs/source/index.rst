..  thekraf documentation master file, created by
    sphinx-quickstart on Sat Feb 27 09:00:13 2016.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

.. toctree::
    intro
    installation
    server
    development
    config
    components
    code

.. include:: ../../README.rst
    :end-before: .. do-not-include-after-this

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

.. * :ref:`search`

