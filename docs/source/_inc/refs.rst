.. _Bootstrap 3: http://getbootstrap.com
.. _CoffeeScript: http://coffeescript.org
.. _deploying-flask: http://flask.pocoo.org/docs/0.10/deploying/
.. _ev-paper: https://
    www.eecis.udel.edu/~mckennar/academics/gametheory/farkle.pdf
.. _Farkle: https://en.wikipedia.org/wiki/Farkle
.. _Farkle Rules: https://
    cdn.shptrn.com/media/mfg/1725/media_document/8976/FarkleRules_E.pdf
.. _Flask: http://flask.pocoo.org
.. _Hamlish-jinja: https://github.com/Pitmairen/hamlish-jinja
.. _Python 3: https://www.python.org
.. _Read the Docs: http://thekraf.readthedocs.org/en/latest/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Sphinx: http://www.sphinx-doc.org/en/stable/index.html
.. _SQLAlchemy: http://www.sqlalchemy.org
.. _sqlite: https://www.sqlite.org
.. _webpack: https://webpack.github.io
