Installation
============

Docker
------

The quickest way to get running is with `docker <https://www.docker.com>`_ (that
is if you already have docker installed).  If this is the case, from the project
root, you can build the project image with the command::

    docker build -t thekraf .

If you already have the python 3.5 image, this takes ~3 minutes.

Then you can run the image/server with::

    docker run --rm -it -p 5000:5000 thekraf

.. note::
    If you are not running on Linux, you'll need the address of your docker
    machine to connect to the server which you can get with a command of the
    form::

        docker-machine ip <machine-name>

Standard Install
----------------

Refer to the :ref:`Quickstart <quickstart>`.
