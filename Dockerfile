# An image can be built in ~3 minutes (assuming the python image is already
# downloaded) from this file if from this directory you run a command like
#   docker build -t thekraf .
# Then, you can run the image with
#   docker run --rm -it -p 5000:5000 thekraf
# Note, if you are not running on linux, you'll need the address of your docker
# machine to connect to server which you can get with a command of the form
#   docker-machine ip <machine-name>

FROM python:3.5.1

ARG tag='v0.1a12'

RUN set -x && \
    # Prep apt so we can install nodejs/npm
    curl -sL https://deb.nodesource.com/setup_5.x | bash - && \
    # Install nodejs/npm
    apt-get install -y nodejs && \
    # Install webpack globally
    npm -g install webpack && \
    # Clone the git repo
    git clone https://bitbucket.org/carbonizer/thekraf && \
    # Check out specific tag
    ( \
        if [ -n "$tag" ]; then \
            git -C thekraf checkout $tag; \
        fi; \
    ) && \
    # Install the pip reqs and gunicorn (the production server)
    # Use --no-cache-dir to keep docker image as slim as possible
    pip3 install --no-cache-dir -r thekraf/requirements.txt gunicorn && \
    # Install the package in dev mode
    pip3 install --no-cache-dir -e "./thekraf[doc,dev,test]" && \

    # NOTE: All of the frontend commands are actually redundant because the
    # pre-built bundle is already included in the repo.  The commands are here
    # for reference and to test the workflow.

    # Change to the web folder
    cd thekraf/thekraf/flaskapp && \
    # Install node dependencies
    npm install && \
    # Clean cache to keep docker image slim
    npm cache clean && \
    # Set NODE_PATH so webpack will work
    export NODE_PATH="$(npm root -g)" && \
    # Build the frontend bundle
    webpack && \
    # 'popd-ing' (pushd/popd aparently aren't part of dash, assuming dash is
    # /bin/sh in jessie) is not really necessary since the directory changes
    # won't persist beyond this docker command.  However, it allows these
    # commands to stand alone should additional command be added later.
    cd ../../.. && \

    # Precache score options
    thekraf reset score_options

EXPOSE 5000

CMD ["/bin/bash", "-c", "eval \"$(thekraf web gunicorn)\""]
